<?php
date_default_timezone_set("America/Argentina/Buenos_Aires");
$servidor = "";
$usuario = "";
$pass = "";
$db = "";

if (!$servidor || !$usuario || !$pass || !$db) {
    echo 'Hace falta configurar los datos de la base de datos en el archivo config.ini';
    die;
}
