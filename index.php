<?php

include 'config.php';

$con = new mysqli($servidor, $usuario, $pass, $db);


$pagina = $_GET['pagina'] ? (int) $_GET['pagina'] : 1;
$filasPorPagina = 10;
$numeroDePaginas = ceil(($con->query("SELECT * FROM temperatura_y_humedad")->num_rows) / $filasPorPagina);

if ($con->connect_error) {
  die("fallo la conexion: " . $con->connect_error);
}

$sql = "SELECT * FROM temperatura_y_humedad ORDER BY fecha_hora desc LIMIT " .
  $filasPorPagina . " OFFSET " . ($pagina - 1) * $filasPorPagina;
$result = $con->query($sql);
$lecturas = [];
if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    $lecturas[] = [
      "mac" => $row["mac"],
      "fecha_hora" => $row["fecha_hora"],
      "temperatura" => $row["temperatura"],
      "humedad" => $row["humedad"],
      "usuario" => $row["usuario"]
    ];
  }
  $fecha_horas = [];
  for ($i = count($lecturas) - 1; $i >= 0; $i--) {
    array_push(
      $fecha_horas,
      date(
        'H:i',
        strtotime(
          $lecturas[$i]['fecha_hora']
        )
      )
    );
  }
  $temperaturas = array_reverse(array_column($lecturas, 'temperatura'));
  $humedades = array_reverse(array_column($lecturas, 'humedad'));
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Temperatura y humedad</title>
  <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body class="bg-light">
  <h1 class="text-center my-3">Temperatura y humedad</h1>
  <div class="container d-flex flex-column">
    <div class="d-flex flex-column flex-lg-row justify-content-lg-around my-4">
      <div class="d-flex flex-column">
        <h3 class="text-center">Temperatura</h3>
        <canvas id="canvasTemperaturas" style="width:27.5vw"></canvas>
      </div>
      <div class="d-flex flex-column">
        <h3 class="text-center">Humedad</h3>
        <canvas id="canvasHumedades" style="width:27.5vw"></canvas>
      </div>
    </div>


    <div class="overflow-auto mt-4">
      <table class="table table-striped">
        <thead>
          <tr class="text-center">
            <th scope="col">MAC</th>
            <th scope="col">Fecha</th>
            <th scope="col">Temperatura</th>
            <th scope="col">Humedad</th>
            <th scope="col">Usuario</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($lecturas as $lectura) {

          ?>
            <tr>
              <th scope="row"><?php echo $lectura["mac"] ?></th>
              <td class="text-nowrap"><?php echo $lectura["fecha_hora"] ?></td>
              <td class="text-nowrap text-center"><?php echo $lectura["temperatura"] ?>°</td>
              <td class="text-nowrap text-center"><?php echo $lectura["humedad"] ?>%</td>
              <td class="text-nowrap text-center"><?php echo $lectura["usuario"] ?></td>

            </tr>
          <?php
          }
          ?>

        </tbody>
      </table>
    </div>
    <nav class="align-self-center mt-4">
      <ul class="pagination">
        <?php if ($pagina > 2) { ?>
          <li class="page-item"><a class="page-link" href="?pagina=<?php echo $pagina - 2 ?>"><?php echo $pagina - 2 ?></a></li>
          <li class="page-item"><a class="page-link" href="?pagina=<?php echo $pagina - 1 ?>"><?php echo $pagina - 1 ?></a></li>
        <?php } else if ($pagina > 1) { ?>
          <li class="page-item"><a class="page-link" href="?pagina=<?php echo $pagina - 1 ?>"><?php echo $pagina - 1 ?></a></li>
        <?php } ?>
        <li class="page-item active"><span class="page-link"><?php echo $pagina ?></span></li>
        <li class="page-item"><a class="page-link" href="?pagina=<?php echo $pagina + 1 ?>"><?php echo $pagina + 1 ?></a></li>
        <li class="page-item"><a class="page-link" href="?pagina=<?php echo $pagina + 2 ?>"><?php echo $pagina + 2 ?></a></li>
      </ul>
    </nav>
  </div>
  <script src="js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.6.0/chart.min.js"></script>
  <script>
    const canvasTemperaturas = document.getElementById('canvasTemperaturas');
    const canvasHumedades = document.getElementById('canvasHumedades');
    const temperaturas = new Chart(canvasTemperaturas, {
      type: 'line',
      data: {
        labels: <?php echo json_encode($fecha_horas); ?>,
        datasets: [{
          label: 'Temperaturas',
          data: <?php echo json_encode($humedades); ?>,
          backgroundColor: [
            'rgba(54, 153, 234, 0.8)',
          ],
          borderColor: [
            'rgba(54, 153, 234, 0.8)',
          ],
          borderWidth: 1,
          fill: {
            target: 'origin',
            above: 'rgba(54, 153, 234, 0.2)', // Area will be red above the origin
            below: 'rgba(54, 153, 234, 0.2)' // And blue below the origin
          },
          tension: 0.2,
        }]
      },
      options: {
        responsive: true
      }
    });
    const humedades = new Chart(canvasHumedades, {
      type: 'line',
      data: {
        labels: <?php echo json_encode($fecha_horas); ?>,
        datasets: [{
          label: 'Humedades',
          data: <?php echo json_encode($temperaturas); ?>,
          backgroundColor: [
            'rgba(49, 168, 73, 0.8)',
          ],

          borderColor: [
            'rgba(49, 168, 73, 0.8)',
          ],
          borderWidth: 1,
          fill: {
            target: 'origin',
            above: 'rgba(49, 168, 73, 0.2)', // Area will be red above the origin
            below: 'rgba(49, 168, 73, 0.2)' // And blue below the origin
          },
          tension: 0.2,
        }]
      },
      options: {
        responsive: true
      }
    });
  </script>
</body>

</html>